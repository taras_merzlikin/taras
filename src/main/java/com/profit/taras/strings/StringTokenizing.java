package com.profit.taras.strings;

import java.util.StringTokenizer;

/**
 * Created by profit on 10.06.17.
 */
public class StringTokenizing {
    /**
     * @param str
     */
    public static void splitBySpace(String str) {
        StringTokenizer st = new StringTokenizer(str);
        System.out.println("---split by space---");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     * @param str
     */
    public static void splitByComma(String str) {

        System.out.println("---split by comma','---");
        StringTokenizer sti = new StringTokenizer(str, ",");

        while (sti.hasMoreElements()) {
            System.out.println(sti.nextElement());
        }

    }
}

