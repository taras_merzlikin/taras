package com.profit.taras.inheritance;

/**
 * Created by profit on 11.06.17.
 */
public class Cylinder extends Circle {

    /**
     *
     */

    private double height;

    public Cylinder() {
        super();
        height = 1.0;
    }

    public Cylinder(double height) {

        super();
        this.height = height;

    }

    public Cylinder(double radius, double height) {
        super();
        this.height = height;

    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return getArea() * height;
    }

    @Override
    public String toString() {
        return " Cylinder: subclass of " + super.toString() + " height = " + height;
    }


}
