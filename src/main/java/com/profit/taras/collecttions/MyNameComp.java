package com.profit.taras.collecttions;

import java.util.Comparator;

/**
 * Created by profit on 09.07.17.
 */
public class MyNameComp implements Comparator<Empl> {
    @Override
    public int compare(Empl e1, Empl e2) {
        return e1.getName().compareTo(e2.getName());
    }
}
