package com.profit.taras.collecttions;

/**
 * Created by profit on 09.07.17.
 */
public class FruitIterator {
    private MyOwnArreyList fruitList;
    private int position;

    public FruitIterator(MyOwnArreyList fruitList) {
        this.fruitList = fruitList;
    }

    public boolean hasNext() {
        if (position < fruitList.size())
            return true;
        else
            return false;
    }

    public Object next() {
        Object anyObj = fruitList.get(position);
        position++;
        return anyObj;
    }

    public void remove() {
        fruitList.remove(position);
    }
}
