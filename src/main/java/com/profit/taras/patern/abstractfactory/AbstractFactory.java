package com.profit.taras.patern.abstractfactory;

import com.profit.taras.patern.builder.MammalFactory;
import com.profit.taras.patern.builder.ReptileFactory;

/**
 * Created by profit on 15.07.17.
 */
public class AbstractFactory {

    public SpeciesFactory getSpeciesFactory(String type) {
        if ("mammal".equals(type)) {
            return new MammalFactory();
        } else {
            return new ReptileFactory();
        }
    }
}
