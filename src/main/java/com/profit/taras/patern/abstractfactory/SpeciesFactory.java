package com.profit.taras.patern.abstractfactory;

import com.profit.taras.patern.factory.Animal;

/**
 * Created by profit on 15.07.17.
 */
public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}
