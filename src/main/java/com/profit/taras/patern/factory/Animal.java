package com.profit.taras.patern.factory;

/**
 * Created by profit on 15.07.17.
 */
public abstract class Animal {
    public abstract String makeSound();
}
