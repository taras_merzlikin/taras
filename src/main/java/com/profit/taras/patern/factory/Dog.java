package com.profit.taras.patern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class Dog extends Animal {
    @Override
    public String makeSound() {
        return "GOU!";
    }
}
