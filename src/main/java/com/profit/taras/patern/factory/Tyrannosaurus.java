package com.profit.taras.patern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class Tyrannosaurus extends Animal {
    @Override
    public String makeSound() {
        return "roooorrrrr";
    }
}
