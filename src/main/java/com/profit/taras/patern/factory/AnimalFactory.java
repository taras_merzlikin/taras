package com.profit.taras.patern.factory;

/**
 * Created by profit on 15.07.17.
 */
public class AnimalFactory {
    public Animal getAnimal(String type){
        if ("canine".equals(type)) {
            return new Dog();
        }else {
            return new Cat();
        }
    }
}
