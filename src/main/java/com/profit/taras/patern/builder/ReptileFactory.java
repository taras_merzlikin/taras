package com.profit.taras.patern.builder;

import com.profit.taras.patern.abstractfactory.SpeciesFactory;
import com.profit.taras.patern.factory.*;

/**
 * Created by profit on 15.07.17.
 */
public class ReptileFactory extends SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if ("snake".equals(type)) {
            return new Snake();
        } else {
            return new Tyrannosaurus();
        }
    }
}