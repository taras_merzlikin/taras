package com.profit.taras.patern.builder;

import com.profit.taras.patern.abstractfactory.SpeciesFactory;
import com.profit.taras.patern.factory.Animal;
import com.profit.taras.patern.factory.Cat;
import com.profit.taras.patern.factory.Dog;

/**
 * Created by profit on 15.07.17.
 */
public class MammalFactory extends SpeciesFactory {
    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
