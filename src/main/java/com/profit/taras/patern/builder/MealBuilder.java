package com.profit.taras.patern.builder;

/**
 * Created by profit on 15.07.17.
 */
public interface MealBuilder {
    void buildDrink();

    void buildMainCourse();

    void buildSide();

    Meal getMeal();

}
