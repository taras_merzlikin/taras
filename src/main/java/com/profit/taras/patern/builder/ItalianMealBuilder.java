package com.profit.taras.patern.builder;

import com.profit.taras.patern.builder.Meal;
import com.profit.taras.patern.builder.MealBuilder;

/**
 * Created by profit on 15.07.17.
 */
public class ItalianMealBuilder implements MealBuilder {
    @Override
    public void buildDrink() {

    }

    @Override
    public void buildMainCourse() {

    }

    @Override
    public void buildSide() {

    }

    @Override
    public Meal getMeal() {
        return null;
    }
}
