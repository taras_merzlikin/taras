package com.profit.taras.patern.prototype;

/**
 * Created by profit on 15.07.17.
 */
public interface Protoype {
    public Protoype doClone();
}
