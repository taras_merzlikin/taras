package com.profit.taras.patern.prototype;

/**
 * Created by profit on 15.07.17.
 */
public class Person implements Protoype {

    String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public Protoype doClone() {
return new  Person(name);
    }

    @Override
    public String toString() {
        return "this person is named" + name;
    }
}
