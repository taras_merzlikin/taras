package com.profit.taras.loopsandarreys;

import java.util.Arrays;

/**
 * 03.06
 */

public class ForLoop {
    /**
     * equality Of Two Arrays
     *
     * @param myFirstArray
     * @param mySecondArray
     */
    public static void equalityOfTwoArrays(int[] myFirstArray, int[] mySecondArray) {

        boolean equalOrNot = true;

        if (myFirstArray.length == mySecondArray.length) {

            for (int i = 0; i < myFirstArray.length; i++) {

                if (myFirstArray[i] != mySecondArray[i]) {
                    equalOrNot = false;

                }
            }
        } else {
            equalOrNot = false;
        }

        if (equalOrNot) {
            System.out.println("Two arrays are equal.");
        } else {
            System.out.println("Two  arrays are not equal.");
        }
    }

    /**
     * @param myArray
     */
    public static void uniqueArray(int[] myArray) {

        int arraySize = myArray.length;

        System.out.println("Original Array : ");

        for (int i = 0; i < arraySize; i++) {
            System.out.print(myArray[i] + "\t");
        }

        //Comparing each element with all other elements
        for (int i = 0; i < arraySize; i++) {

            for (int j = i + 1; j < arraySize; j++) {

                //If any two elements are found equal
                if (myArray[i] == myArray[j]) {

                    //Replace duplicate element with last unique element
                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;

                    j--;
                }
            }
        }

        //Copying only unique elements of my_array into array1

        int[] arrayResult = Arrays.copyOf(myArray, arraySize);

        //Printing arrayWithoutDuplicates
        System.out.println();

        System.out.println("Array with unique values : ");

        for (int i = 0; i < arrayResult.length; i++) {
            System.out.print(arrayResult[i] + "\t");
        }

    }
}
