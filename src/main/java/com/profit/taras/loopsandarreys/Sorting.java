package com.profit.taras.loopsandarreys;

/**
 * Created by profit on 04.06.17.
 */
public class Sorting {
    /**
     * arrey 2 1 4 3
     *
     * @param num
     */
    public static void selectionSort(int[] num) {

        int first;
        int temp;

        for (int i = num.length - 1; i > 0; i--) {
            //initialize of first element

            first = 0;
            for (int j = 0; j <= i; j++) {
                if (num[j] < num[first]) {
                    first = j;
                }

            }
            temp = num[first];
            num[first] = num[i];
            num[i] = temp;
        }

    }
}
