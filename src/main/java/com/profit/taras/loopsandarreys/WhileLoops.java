package com.profit.taras.loopsandarreys;

import static com.profit.taras.firsapp.FirstApp.*;

/**
 * Created by profit on 28.05.17.
 */
public class WhileLoops {
    /**
     * sellecting choise
     *
     * @param str
     *
     */
    public static void selectChoise(String str) {

        String choise;
        String con = "y";

        println("what is the command keyword to exit a loop in a Java?");

        println("a.quit");
        println("b.continue");
        println("c.break");
        println("d.exit");

        while (con.compareTo("y") == 0) {
            println("enter your choice");

            choise = str;

            if (choise.compareTo("c") == 0) {
                println("congratulation!");
                break;

            } else if (choise.compareTo("q") == 0 || choise.compareTo("e") == 0) {
                println("exiting...!");
                break;
            } else {
                println("incorrect");
                print("again? pres to continue");
                break;
            }

        }
    }
}
