package com.profit.taras.loopsandarreys;

/**
 * Created by profit on 03.06.17.
 */
public class Matrix {
    /**
     * @param first
     * @param second
     * @return
     * @throws IllegalAccessException
     */
    public static Double[][] multiplicar(Double[][] first, Double[][] second) throws IllegalAccessException {
        int firstRows = first.length;
        int firstColums = first[0].length;
        int secondRows = second.length;
        int secondColums = second[0].length;
        if (firstColums != secondRows) {
            throw new IllegalAccessException("First Rows: " + firstColums
                    + " didnot math Second Colums " + secondRows + ".");
        }
        Double[][] result = new Double[firstRows][secondColums];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                result[i][j] = 0.00000;
            }

        }
        for (int i = 0; i < firstRows; i++) {
            for (int j = 0; j < secondColums; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += first[i][k] * second[k][j];

                }

            }

        }

        return result;
    }
}
