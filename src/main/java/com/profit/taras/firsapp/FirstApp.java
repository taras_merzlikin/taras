package com.profit.taras.firsapp;

import static java.lang.System.*;

/**
 * Created by profit on 27.05.17.
 */
public class FirstApp {

    /**
     * print method
     * @param t
     * @param <T>
     */
    public static <T> void print(T t) {
        out.print(t);
    }

    /**
     * println method that prints Object
     * @param object
     */

    public static void println(Object object) {
        out.println(object);

    }

    /**
     * println method that prints new line
     */

    public static void println() {
        out.println();

    }

}
