package com.profit.taras.exception;

/**
 * Created by profit on 02.07.17.
 */
public class NameNotFoundException extends Exception{
    private int errCode;
    public NameNotFoundException(int errCode, String massage){
        super(massage);
        this.errCode=errCode;
    }
    public int getErrCode(){
        return errCode;
    }
    public void setErrCode(int errCode){
        this.errCode=errCode;
    }
}
