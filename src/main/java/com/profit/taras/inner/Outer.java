package com.profit.taras.inner;

/**
 * Created by profit on 01.07.17.
 */
public class Outer {
    private static String textNested = "NestedString!";
private String text = "i am private!";

    public static class  Nested{
        public void printNestedText(){
            System.out.println(textNested);
        }
    }
    public class Inner{
        private String text = "i am inner private";
        public void printText(){
            System.out.println(text);
            System.out.println(Outer.this.text);
        }
    }
    public void local(){
        class Local{
             }
             Local local = new Local();
    }
    public void doIt(){
        System.out.println("OuterClass doIt()");
    }
}

