package com.profit.taras.datatypes;

/**
 * Created by profit on 28.05.17.
 */
public class DataTypes {
    /**
     * tipe byte
     */
    private byte aByte;
    /**
     * tipe short
     */
    private short aShort;
    /**
     * tipe integer
     */
    private int anInt;
    /**
     * tipe long
     */
    private long aLong;
    /**
     * tipe boolean
     */
    private boolean aBoolean;
    /**
     * tipe char
     */
    private char aChar;
    /**
     * type float
     */
    private float aFloat;
    /**
     * tupe double
     */
    private double aDouble;

    /**
     * get tipe byte
     * @return
     */
    public byte getaByte() {
        return aByte;
    }

    /**
     * get type short
     * @return
     */
    public short getaShort() {
        return aShort;
    }

    /**
     * get type integer
     * @return
     */
    public int getAnInt() {
        return anInt;
    }

    /**
     * get type long
     * @return
     */
    public long getaLong() {
        return aLong;
    }

    /**
     * get type boolean
     * @return
     */
    public boolean isaBoolean() {
        return aBoolean;
    }

    /**
     * get tipe char
     * @return
     */
    public char getaChar() {
        return aChar;

    }

    /**
     * get type float
     * @return
     */
    public float getaFloat() {
        return aFloat;
    }

    /**
     * get type double
     * @return
     */
    public double getaDouble() {
        return aDouble;
    }

    public DataTypes(byte aByte, short aShort, int anInt, long aLong,
                     boolean aBoolean, char aChar, float aFloat, double aDouble) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aBoolean = aBoolean;
        this.aChar = aChar;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
    }
}


