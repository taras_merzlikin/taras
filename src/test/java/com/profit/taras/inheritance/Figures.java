package com.profit.taras.inheritance;

import org.junit.Test;

/**
 * Created by profit on 11.06.17.
 */
public class Figures {
    @Test
    public void getArea() {
        Circle circle = new Circle();
        System.out.println(circle.toString());


    }

    @Test
    public void getVolume() {
        Cylinder cylinder = new Cylinder();
        System.out.println(cylinder.toString());

        Circle circle = new Circle(4.5);
        System.out.println(circle.getArea());


    }

    @Test
    public void cylinderData() {

        System.out.println("----------------cilinder-------------------------");

        Cylinder cylinder = new Cylinder();
        System.out.println(cylinder.getHeight());
        System.out.println(cylinder.getVolume());
        System.out.println(cylinder.getArea());
        System.out.println(cylinder.getRadius());
        System.out.println(cylinder.toString());

        System.out.println("----------------cilinder 1 -------------------------");

        Cylinder cylinder1 = new Cylinder(5);
        System.out.println(cylinder1.getHeight());
        System.out.println(cylinder1.getVolume());
        System.out.println(cylinder1.toString());

        System.out.println("----------------cilinder 2 -------------------------");

        Cylinder cylinder2 = new Cylinder(4.5, 5);
        System.out.println(cylinder2.getHeight());
        System.out.println(cylinder2.getVolume());
        System.out.println(cylinder2.getArea());
        System.out.println(cylinder2.getRadius());
        System.out.println(cylinder2.toString());

        System.out.println("----------------cilinder-------------------------");
    }
}
