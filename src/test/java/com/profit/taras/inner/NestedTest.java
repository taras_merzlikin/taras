package com.profit.taras.inner;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

/**
 * Created by profit on 01.07.17.
 */
@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void beforeAllTestMethods() {
        System.out.println("Invoked");
    }

    @Before
    public void beforeEachTestMethods() {
        System.out.println("Invoked before each TestMethods");
    }

    @After
    public void afterEachTestMethods() {
        System.out.println("Invoked after");
    }

    @AfterClass
    public static void afterAllTestMethods() {
        System.out.println("Invoked after all");
    }

    @Test
    public void rootClassTest() {
        System.out.println("root class test");
    }

    public class ContrxtA {
        @Before
        public void beforeEachTestMethodsOfContextA() {
            System.out.println("Invoked before each TestMethods");
        }

        @After
        public void afterEachTestMethodsOfContextA() {
            System.out.println("Invoked after");
        }

        @Test
        public void contextATest() {
            System.out.println("root class test");
        }

        public class ContrxtC {
            @Before
            public void beforeEachTestMethodsOfContextC() {
                System.out.println("Invoked before each TestMethods");
            }

            @After
            public void afterEachTestMethodsOfContextC() {
                System.out.println("Invoked after");
            }

            @Test
            public void contextCTest() {
                System.out.println("root class test");
            }
        }
    }
}

