package com.profit.taras.inner;

import org.junit.Test;

/**
 * Created by profit on 01.07.17.
 */
public class CacheTest {
    @Test
    public void cacheTest(){
        String key = "Pasword";
        Integer pass = 1567888;

        Cache cache = new Cache();
        cache.store(key, pass);

        System.out.println(cache.get("Pasword"));
        System.out.println(cache.getData("Pasword"));
    }
}
