package com.profit.taras.inner;

import org.junit.Test;

/**
 * Created by profit on 01.07.17.
 */
public class OuterTest {
    @Test
    public void outerTest(){
        Outer.Nested nested = new Outer.Nested();
       Outer outer = new Outer();
       Outer.Inner inner = outer.new Inner();
       inner.printText();
       outer.doIt();

       Outer outer1 = new Outer(){
           public void doIt(){
               System.out.println("anonimous class doIt()");
           }
       };
       outer1.doIt();
    }
    @Test
    public void myIntTeast(){

        final String textToPrint = "Text...";

        MyInterface myInterface = new MyInterface() {

            private String text;

            {this.text = textToPrint;}
            @Override
            public void doIt() {
                System.out.println("anonimus class doIt()");
                System.out.println(this.text);
            }
        };
        myInterface.doIt();
    }
    @Test
    public void local(){
        LocalInner obj = new LocalInner();
        obj.display();
    }
}
