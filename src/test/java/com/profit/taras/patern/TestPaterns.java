package com.profit.taras.patern;

import com.profit.taras.patern.abstractfactory.AbstractFactory;
import com.profit.taras.patern.abstractfactory.SpeciesFactory;
import com.profit.taras.patern.builder.*;
import com.profit.taras.patern.factory.Animal;
import com.profit.taras.patern.factory.AnimalFactory;
import com.profit.taras.patern.prototype.Dog;
import com.profit.taras.patern.prototype.Person;
import com.profit.taras.patern.singleton.SingletonExample;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by profit on 15.07.17.
 */
public class TestPaterns {
    @Test
    public void builder(){
        Student s = new Student.Builder().name("alexey").age(18).language(Arrays.asList("chinees","english")).build();
        System.out.println(s);
    }
    @Test
    public void singleton(){
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.sayHello();
    }

    @Test
            public void factory() {
        AnimalFactory animalFactory = new AnimalFactory();
        Animal a1 = animalFactory.getAnimal("feline");
        System.out.println("a1 sound: " + a1.makeSound());
        Animal a2 = animalFactory.getAnimal("canine");
        System.out.println("a2 sound: " + a2.makeSound());
    }
    @Test

        public void abstractFactory() {
            AbstractFactory abstractFactory = new AbstractFactory();

            SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");
            Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
            System.out.println("a1 sound: " + a1.makeSound());
            Animal a2 = speciesFactory1.getAnimal("snake");
            System.out.println("a2 sound: " + a2.makeSound());

            Animal animal = abstractFactory.getSpeciesFactory("reptile").getAnimal("snake");
            System.out.println("reptile sound: " + animal.makeSound());

            SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");
            Animal a3 = speciesFactory2.getAnimal("dog");
            System.out.println("a3 sound: " + a3.makeSound());
            Animal a4 = speciesFactory2.getAnimal("cat");
            System.out.println("a4 sound: " + a4.makeSound());
        }

    @Test
    public  void prototype() {

        Person person1 = new Person("Fred");
        System.out.println("person 1:" + person1);
        Person person2 = (Person) person1.doClone();
        System.out.println("person 2:" + person2);

        Dog dog1 = new Dog("Wooof!");
        System.out.println("dog 1:" + dog1);
        Dog dog2 = (Dog) dog1.doClone();
        System.out.println("dog 2:" + dog2);
    }

    @Test
    public void builder2() {
        MealBuilder mealBuilder = new ItalianMealBuilder();
        MealDirector mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        Meal meal = mealDirector.getMeal();
        System.out.println("meal is: " + meal);

        mealBuilder = new JapaneseMealBuilder();
        mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        meal = mealDirector.getMeal();
        System.out.println("meal is: " + meal);
    }
}

