package com.profit.taras.loopsandarreys;

import org.junit.Test;
import com.profit.taras.loopsandarreys.Matrix.*;

/**
 * Created by profit on 03.06.17.
 */
public class MatrixTest {
    @Test
    public void matrixTesting() throws IllegalAccessException {

        Double[][] firstMatrix = {
                {4.12, 5.25, 6.75},
                {5.35, 4.12, 3.76},
                {6.34, 2.13, 2.32}
        };
        Double[][] secondMatrix = {
                {5.12, 4.25, 1.75},
                {8.35, 1.12, 5.76},
                {9.34, 3.13, 2.32}
        };
        Double[][] resdult = Matrix.multiplicar(firstMatrix, secondMatrix);
        for (int i = 0; i < resdult.length; i++) {
            for (int j = 0; j < resdult.length; j++) {
                System.out.print(resdult[i][j] + " ");
            }
            System.out.println();

        }

    }
}
