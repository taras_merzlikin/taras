package com.profit.taras.loopsandarreys;
import com.profit.taras.loopsandarreys.ForLoop.*;


import org.junit.Test;

import static com.profit.taras.firsapp.FirstApp.println;
import static com.profit.taras.loopsandarreys.ForLoop.equalityOfTwoArrays;
import static com.profit.taras.loopsandarreys.ForLoop.uniqueArray;

public class ForLoopTest {
     @Test
    public void forLoopTesting(){
         int[]x={5,6,7,8};
         int[]y={5,6,7,8,7};
         int[]z={5,6,7,9};

         equalityOfTwoArrays(x,y);
         equalityOfTwoArrays(y,z);

         println();

         uniqueArray(x);

         println();

         uniqueArray(y);


     }

}
