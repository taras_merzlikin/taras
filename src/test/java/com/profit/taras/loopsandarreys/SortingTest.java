package com.profit.taras.loopsandarreys;

import org.junit.Test;

import java.util.Arrays;

import static com.profit.taras.loopsandarreys.Sorting.selectionSort;

/**
 * Created by profit on 04.06.17.
 */
public class SortingTest {
    @Test
    public void sortingTesting(){
        int [] num= {2,1,4,3};

        selectionSort(num);
        System.out.print(Arrays.toString(num));
    }

}
