package com.profit.taras.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by profit on 25.06.17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CalculatorTest.class,
        CalculatorTest2.class
})
public class AllTests {

    @Test
    public void test(){
        System.out.println("all classes");
    }
}
