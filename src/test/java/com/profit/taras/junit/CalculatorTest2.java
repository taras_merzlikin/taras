package com.profit.taras.junit;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by profit on 25.06.17.
 */
public class CalculatorTest2 {
    @BeforeClass
    public static void setUpbeforeClass() throws Exception {
        System.out.println("before class");

    }
    @Before
    public void setUp() throws Exception{
        System.out.println("before");
    }
    @Test
    public void testFindMax(){
        System.out.println("test case find max");
               assertEquals(4, Calculator.findMax(new int[]{1,3,2,4}));
                assertEquals(-1, Calculator.findMax(new int[]{-12,-3,-1,-4,-2}));
    }
    public void testCube(){
        System.out.println("test case cube");
        assertEquals(27, Calculator.cube(3));
    }
    @Test
    public void testReverseWord(){
        System.out.println( "test case ReverseWord");
        assertEquals("ym eman si nahk ", Calculator.reverseWord("my name is khan"));
    }
    @After
    public void tearDuwn() throws Exception{
        System.out.println("after");
    }
    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        System.out.println("after class");
    }
}
