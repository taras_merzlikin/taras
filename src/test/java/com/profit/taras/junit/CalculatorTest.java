package com.profit.taras.junit;

import org.junit.Test;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by profit on 25.06.17.
 */
public class CalculatorTest {
    @Test
    public void testFindMax(){
        assertEquals(4, Calculator.findMax(new int[]{1,3,2,4}));
        assertEquals(-1, Calculator.findMax(new int[]{-12,-3,-1,-4,-2}));

    }
    @Test
    public void multiplicationZeroIntegersShouldReturnZerro(){
        Calculator calculator=new Calculator();

        assertEquals("10*0 must be 0", 0, calculator.multiply(10,0));

        assertEquals("0*10 must be 0", 0, calculator.multiply(0,10));
        assertEquals("0*0 must be 0", 0, calculator.multiply(0,0));
    }
}
