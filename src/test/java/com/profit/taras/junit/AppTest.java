package com.profit.taras.junit;



import org.junit.jupiter.api.*;

import java.util.function.Supplier;

/**
 * Created by profit on 25.06.17.
 */
public class AppTest {
    @BeforeAll
    static void setup(){
        System.out.println("@BeforAll executed");
    }
    @BeforeEach
    void setupThis(){
        System.out.println("@BeforeEach executed");
    }
    @Tag("DEV")
    @Test
    void testCalcOne(){
        System.out.println("_______-=Test one executed=-________");
        Assertions.assertEquals (6, 2, 4);
    }
    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo(){
        System.out.println("_______-=Test two executed=-________");
        Assertions.assertEquals (6, 2, 4);
    }

    @AfterEach
    void tearThis(){
        System.out.println("@AfterEach executed");
    }
    @AfterAll
    static void tear(){
        System.out.println("@AfterAll executed");
    }
    @Tag("DEV")
    @Test
    void testCase(){
        Assertions.assertNotEquals(3, Calculator.sum(2, 2));

        Assertions.assertNotEquals(5, Calculator.sum(2, 2),"calculator test summ 2+2-test failed") ;

        Supplier<String> messegeSupplier = ()->"calculator test summ 2+2-test failed";

        Assertions.assertNotEquals(5, Calculator.sum(2, 2), messegeSupplier);

    }
}
