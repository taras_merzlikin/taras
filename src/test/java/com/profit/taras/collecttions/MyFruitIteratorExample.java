package com.profit.taras.collecttions;

import org.junit.Test;

/**
 * Created by profit on 09.07.17.
 */
public class MyFruitIteratorExample {
    @Test
    public void test() {
        MyOwnArreyList fruitList = new MyOwnArreyList();
        fruitList.add("mango");
        fruitList.add("strawaberry");
        fruitList.add("papaya");
        fruitList.add("watermalon");
        System.out.println("----calling my iterator on my ArreyList----");
        FruitIterator it = fruitList.iterator();
        while (it.hasNext()) {
            String s = (String) it.next();
            System.out.println("value: " + s);
        }
        System.out.println("--Fruit List size: " + fruitList.size());
        fruitList.remove(1);
        System.out.println("--after removal, fruit list size: " + fruitList.size());

        for (int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));

        }

    }
}
