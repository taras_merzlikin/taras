package com.profit.taras.collecttions;

import org.junit.Test;

import java.util.TreeSet;

/**
 * Created by profit on 09.07.17.
 */
public class MyComplUserDefine {
    @Test
    public void test() {
        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());

        nameComp.add(new Empl("ram", 3000));
        nameComp.add(new Empl("John", 6000));
        nameComp.add(new Empl("Crish", 2000));
        nameComp.add(new Empl("Tom", 2400));
        for (Empl e : nameComp) {
            System.out.println(e);
        }

        System.out.println("=======================================");
        TreeSet<Empl> salComp = new TreeSet<>(new MySalaryComp());
        nameComp.add(new Empl("ram", 3000));
        nameComp.add(new Empl("John", 6000));
        nameComp.add(new Empl("Crish", 2000));
        nameComp.add(new Empl("Tom", 2400));
        for (Empl e : nameComp) {
            System.out.println(e);
        }
    }
}
